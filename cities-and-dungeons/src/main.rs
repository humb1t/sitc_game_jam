extern crate glutin_window;
extern crate graphics;
extern crate opengl_graphics;
extern crate piston;
extern crate piston_window;
extern crate rand;

use glutin_window::GlutinWindow as Window;
use opengl_graphics::{GlGraphics, OpenGL, Texture};
use piston::event_loop::*;
use piston::input::*;
use piston::window::WindowSettings;
use rand::prelude::*;

const INITIAL_PLAYER_SIZE: f64 = 50.0;
const STEP_SIZE: f64 = 55.0;
const GRAVITY: f64 = 50.0;
const GREEN: [f32; 4] = [0.0, 1.0, 0.0, 1.0];
const RED: [f32; 4] = [1.0, 0.0, 0.0, 1.0];
const BLUE: [f32; 4] = [0.0, 0.0, 1.0, 1.0];
const DUNGEON_BACKGROUND_COLOR: [f32; 4] = [250.0, 100.0, 0.0, 1.0];
const CITY_BACKGROUND_COLOR: [f32; 4] = [222.0, 222.0, 222.0, 1.0];
const WIDTH: u32 = 640;
const HEIGHT: u32 = 640;
const LEVEL_SIZE: usize = 200;
const SCORE_DIFF_IN_CITY: f64 = 10.0;

#[derive(PartialEq)]
enum State {
    Dungeon,
    City,
    Pause,
    End,
}

pub struct Game {
    global: GlobalThings,
    dungeon: Dungeon,
    city: City,
}

pub struct GlobalThings {
    gl: GlGraphics,
    score: f64,
    player_size: f64,
    player_x: f64,
    player_speed_x: f64,
    player_y: f64,
    enemy_x: [f64; 10],
    enemy_y: [f64; 10],
    state: State,
    landscape: Landscape,
}

impl Game {
    fn update(&mut self, args: &UpdateArgs) {
        if self.global.score.is_sign_negative()
            || self.global.player_x > self.global.landscape.dots_x[LEVEL_SIZE - 2]
        {
            self.global.state = State::End;
        }
        if self.global.state == State::Dungeon {
            self.dungeon.update(&mut self.global, args);
        } else if self.global.state == State::City {
            self.city.update(&mut self.global, args);
        }
    }
}

pub struct Landscape {
    dots_x: [f64; LEVEL_SIZE],
    dots_y: [f64; LEVEL_SIZE],
    speed_x: f64,
}

impl Landscape {
    fn new() -> Self {
        let mut dots_x: [f64; LEVEL_SIZE] = [1.0; LEVEL_SIZE];
        let mut dots_y: [f64; LEVEL_SIZE] = [0.0; LEVEL_SIZE];
        for i in 1..LEVEL_SIZE - 1 {
            dots_x[i] = dots_x[i - 1] + STEP_SIZE;
            if (i % 2) != 0 {
                dots_y[i] = dots_y[i - 1];
            } else {
                if random() && ((dots_y[i - 1] + STEP_SIZE) <= HEIGHT as f64) {
                    dots_y[i] = dots_y[i - 1] + STEP_SIZE;
                } else if (dots_y[i - 1] - STEP_SIZE) >= HEIGHT as f64 {
                    dots_y[i] = dots_y[i - 1] - STEP_SIZE;
                }
            }
        }
        Landscape {
            dots_x: dots_x,
            dots_y: dots_y,
            speed_x: 0.0,
        }
    }

    fn find_ground_level_at_x(&self, object_x: f64) -> Option<f64> {
        if let Some(i) = self.dots_x.iter().position(|&x| object_x - x < STEP_SIZE) {
            Some(self.dots_y[i])
        } else {
            None
        }
    }

    fn is_above_ground(&self, object_x: f64, object_y: f64, object_size: f64) -> bool {
        if let Some(dot_y) = self.find_ground_level_at_x(object_x) {
            if (object_y + object_size / 2.0) >= dot_y {
                return false;
            }
        }
        true
    }

    fn is_below_ground(&self, object_x: f64, object_y: f64, object_size: f64) -> bool {
        if let Some(dot_y) = self.find_ground_level_at_x(object_x) {
            if (object_y - object_size / 2.0) <= dot_y {
                return false;
            }
        }
        true
    }

    fn is_on_the_ground(&self, object_x: f64, object_y: f64) -> Option<f64> {
        if let Some(y) = self.find_ground_level_at_x(object_x) {
            if y <= (object_y + 30.0) && y >= (object_y - 30.0) {
                return Some(y);
            }
        }
        None
    }

    fn flip(&mut self) {
        for y in self.dots_y.iter_mut() {
            *y = HEIGHT as f64 - *y;
        }
    }

    fn update_speed(&mut self, object_x: f64) {
        self.speed_x = object_x.exp().min(100.0);
    }
}

pub struct Dungeon {
    daemon_texture: Texture,
    angel_texture: Texture,
    dungeon_landscape_texture: Texture,
}

impl Dungeon {
    fn new(
        daemon_texture: Texture,
        angel_texture: Texture,
        dungeon_landscape_texture: Texture,
    ) -> Self {
        Dungeon {
            daemon_texture: daemon_texture,
            angel_texture: angel_texture,
            dungeon_landscape_texture: dungeon_landscape_texture,
        }
    }
}

pub struct City {
    angel_texture: Texture,
    city_landscape_texture: Texture,
}

impl City {
    fn new(angel_texture: Texture, city_landscape_texture: Texture) -> Self {
        City {
            angel_texture: angel_texture,
            city_landscape_texture: city_landscape_texture,
        }
    }
}

trait Logic {
    fn handle_press_event(&mut self, global: &mut GlobalThings, button: piston::input::Button);
    fn render(&mut self, global: &mut GlobalThings, args: &RenderArgs);
    fn update(&mut self, global: &mut GlobalThings, args: &UpdateArgs);
}

impl Logic for Dungeon {
    fn handle_press_event(&mut self, global: &mut GlobalThings, button: piston::input::Button) {
        if Button::Keyboard(Key::S) == button {
            if let Some(ground_y) = global
                .landscape
                .is_on_the_ground(global.player_x, global.player_y)
            {
                global.player_y = ground_y + global.player_size + STEP_SIZE;
            }
        }
        if Button::Keyboard(Key::D) == button
            && global
                .landscape
                .is_on_the_ground(global.player_x, global.player_y)
                .is_some()
        {
            global.player_speed_x = STEP_SIZE;
        }
        if Button::Keyboard(Key::A) == button
            && global
                .landscape
                .is_on_the_ground(global.player_x, global.player_y)
                .is_some()
        {
            global.player_speed_x = -STEP_SIZE;
        }
    }

    fn render(&mut self, global: &mut GlobalThings, args: &RenderArgs) {
        use graphics::*;
        let player_size = global.player_size;
        let (x, y) = (global.player_x, global.player_y);
        let (dots_x, dots_y) = (global.landscape.dots_x, global.landscape.dots_y);
        let (enemy_x, enemy_y) = (global.enemy_x, global.enemy_y);
        global.gl.draw(args.viewport(), |c, gl| {
            clear(DUNGEON_BACKGROUND_COLOR, gl);
            Image::new()
                .rect(rectangle::square(0.0, 0.0, player_size))
                .draw(
                    &self.angel_texture,
                    &DrawState::default(),
                    c.transform.trans(x, y).trans(-25.0, -25.0),
                    gl,
                );
            for i in 0..LEVEL_SIZE - 2 {
                line_from_to(
                    RED,
                    1.0,
                    [dots_x[i], dots_y[i]],
                    [dots_x[i + 1], dots_y[i + 1]],
                    c.transform.trans(0.0, 0.0),
                    gl,
                );
                Image::new()
                    .rect(
                        [dots_x[i], 0.0, dots_x[i + 1] - dots_x[i], dots_y[i]],
                        //[dots_x[i], dots_y[i], dots_x[i+1]-dots_x[i], HEIGHT as f64 - dots_y[i]],
                    )
                    .draw(
                        &self.dungeon_landscape_texture,
                        &DrawState::default(),
                        c.transform.trans(0.0, 0.0),
                        gl,
                    );
            }
            for i in 0..9 {
                Image::new()
                    .rect(rectangle::square(enemy_x[i], enemy_y[i], 20.0))
                    .draw(
                        &self.daemon_texture,
                        &DrawState::default(),
                        c.transform.trans(0.0, 0.0),
                        gl,
                    );
            }
        });
    }

    fn update(&mut self, global: &mut GlobalThings, args: &UpdateArgs) {
        global.score += 100.0 * args.dt;
        global.player_x = (global.player_x + global.player_speed_x * args.dt)
            .min(WIDTH as f64 - global.player_size);
        for dot_x in global.landscape.dots_x.iter_mut() {
            *dot_x = *dot_x - global.landscape.speed_x * args.dt;
        }
        for i in 0..9 {
            if global.enemy_x[i] <= global.player_x {
                global.enemy_x[i] =
                    global.enemy_x[i] + (global.player_x - global.enemy_x[i]) / (1000.0 * i as f64);
            } else {
                global.enemy_x[i] =
                    global.enemy_x[i] - (global.enemy_x[i] - global.player_x) / (1000.0 * i as f64);
            }
            if global.enemy_y[i] >= global.player_y {
                global.enemy_y[i] =
                    global.enemy_y[i] - (global.enemy_y[i] - global.player_y) / 1000.0;
            } else {
                global.enemy_y[i] =
                    global.enemy_y[i] + (global.player_y - global.enemy_y[i]) / 1000.0;
            }
            if is_collides(
                global.enemy_x[i],
                global.player_x,
                global.enemy_y[i],
                global.player_y,
                global.player_size,
            ) {
                global.score -= 110.0 * args.dt;
                global.player_size -= 1.0 * args.dt;
            }
        }
        if global
            .landscape
            .is_below_ground(global.player_x, global.player_y, global.player_size)
        {
            global.player_y -= GRAVITY * args.dt;
        } else if global
            .landscape
            .is_on_the_ground(global.player_x, global.player_y)
            .is_some()
        {
            global.landscape.update_speed(global.player_x);
        }
    }
}

fn is_collides(
    one_object_x: f64,
    another_object_x: f64,
    one_object_y: f64,
    another_object_y: f64,
    another_object_size: f64,
) -> bool {
    let mut collision_x = false;
    let mut collision_y = false;
    if one_object_x <= another_object_x {
        if (another_object_x - one_object_x) <= another_object_size {
            collision_x = true;
        }
    } else {
        if (one_object_x - another_object_x) <= another_object_size {
            collision_x = true;
        }
    }
    if one_object_y >= another_object_y {
        if (one_object_y - another_object_y) <= another_object_size {
            collision_y = true;
        }
    } else {
        if (another_object_y - one_object_y) <= another_object_size {
            collision_y = true;
        }
    }
    collision_x && collision_y
}

impl Logic for City {
    fn handle_press_event(&mut self, global: &mut GlobalThings, button: piston::input::Button) {
        if Button::Keyboard(Key::W) == button {
            if let Some(ground_y) = global
                .landscape
                .is_on_the_ground(global.player_x, global.player_y)
            {
                global.player_y = ground_y - (global.player_size + STEP_SIZE);
            }
        }
        if Button::Keyboard(Key::D) == button
            && global
                .landscape
                .is_on_the_ground(global.player_x, global.player_y)
                .is_some()
        {
            global.player_speed_x = STEP_SIZE;
        }
        if Button::Keyboard(Key::A) == button
            && global
                .landscape
                .is_on_the_ground(global.player_x, global.player_y)
                .is_some()
        {
            global.player_speed_x = -STEP_SIZE;
        }
    }

    fn render(&mut self, global: &mut GlobalThings, args: &RenderArgs) {
        use graphics::*;
        let player_size = global.player_size;
        let (x, y) = (global.player_x, global.player_y);
        let (dots_x, dots_y) = (global.landscape.dots_x, global.landscape.dots_y);
        global.gl.draw(args.viewport(), |c, gl| {
            clear(CITY_BACKGROUND_COLOR, gl);
            Image::new()
                .rect(rectangle::square(0.0, 0.0, player_size))
                .draw(
                    &self.angel_texture,
                    &DrawState::default(),
                    c.transform.trans(x, y).trans(-25.0, -25.0),
                    gl,
                );
            for i in 0..LEVEL_SIZE - 2 {
                Image::new()
                    .rect([
                        dots_x[i],
                        dots_y[i],
                        dots_x[i + 1] - dots_x[i],
                        HEIGHT as f64 - dots_y[i],
                    ])
                    .draw(
                        &self.city_landscape_texture,
                        &DrawState::default(),
                        c.transform.trans(0.0, 0.0),
                        gl,
                    );
                let transform_line = c.transform.trans(0.0, 0.0);
                line_from_to(
                    RED,
                    1.0,
                    [dots_x[i], dots_y[i]],
                    [dots_x[i + 1], dots_y[i + 1]],
                    transform_line,
                    gl,
                );
            }
        });
    }

    fn update(&mut self, global: &mut GlobalThings, args: &UpdateArgs) {
        global.score -= SCORE_DIFF_IN_CITY * args.dt;
        for dot_x in global.landscape.dots_x.iter_mut() {
            *dot_x = *dot_x - global.landscape.speed_x * args.dt;
        }
        if global.player_size < INITIAL_PLAYER_SIZE {
            global.player_size += 1.0 * args.dt;
        }
        if global
            .landscape
            .is_above_ground(global.player_x, global.player_y, global.player_size)
        {
            global.player_y += GRAVITY * args.dt;
        } else {
            global.landscape.update_speed(global.player_x);
        }
    }
}

fn main() {
    use graphics::*;
    use std::path::Path;

    let opengl = OpenGL::V3_2;
    let mut window: Window = WindowSettings::new("spinning-square", [WIDTH, HEIGHT])
        .graphics_api(opengl)
        .exit_on_esc(true)
        .build()
        .unwrap();

    let mut glyph_cache = opengl_graphics::GlyphCache::new(
        "FiraSans-Regular.ttf",
        (),
        piston_window::TextureSettings::new(),
    )
    .unwrap();
    let daemon_texture = Texture::from_path(
        Path::new("daemon_from_dungeon.png"),
        &piston_window::TextureSettings::new(),
    )
    .unwrap();
    let dungeon_landscape_texture = Texture::from_path(
        Path::new("dungeon_landscape.png"),
        &piston_window::TextureSettings::new(),
    )
    .unwrap();
    let city_landscape_texture = Texture::from_path(
        Path::new("city_landscape_texture.png"),
        &piston_window::TextureSettings::new(),
    )
    .unwrap();
    let angel_texture_1 = Texture::from_path(
        Path::new("angel_from_dungeon.png"),
        &piston_window::TextureSettings::new(),
    )
    .unwrap();
    let angel_texture_2 = Texture::from_path(
        Path::new("angel_from_dungeon.png"),
        &piston_window::TextureSettings::new(),
    )
    .unwrap();

    let mut game = Game {
        global: GlobalThings {
            gl: GlGraphics::new(opengl),
            score: 0.0,
            player_size: INITIAL_PLAYER_SIZE,
            player_x: 225.0,
            player_speed_x: 0.0,
            player_y: 100.0,
            enemy_x: [10.0; 10],
            enemy_y: [300.0; 10],
            state: State::Dungeon,
            landscape: Landscape::new(),
        },
        dungeon: Dungeon::new(daemon_texture, angel_texture_1, dungeon_landscape_texture),
        city: City::new(angel_texture_2, city_landscape_texture),
    };
    let mut events = Events::new(EventSettings::new());

    while let Some(e) = events.next(&mut window) {
        if let Some(b) = e.press_args() {
            if Button::Keyboard(Key::Q) == b {
                break;
            }
            if Button::Keyboard(Key::P) == b {
                if game.global.state != State::Pause {
                    game.global.state = State::Pause;
                } else if game.global.state == State::Pause {
                    game.global.state = State::City;
                }
            }
            if Button::Keyboard(Key::R) == b {
                if game.global.state == State::Dungeon {
                    game.global.landscape.flip();
                    game.global.state = State::City;
                } else if game.global.state == State::City {
                    game.global.landscape.flip();
                    game.global.state = State::Dungeon;
                }
            }
            if game.global.state == State::Dungeon {
                game.dungeon.handle_press_event(&mut game.global, b);
            } else if game.global.state == State::City {
                game.city.handle_press_event(&mut game.global, b);
            }
        }
        if game.global.state == State::Pause {
            continue;
        }
        if let Some(r) = e.render_args() {
            let score = game.global.score;
            game.global.gl.draw(r.viewport(), |c, gl| {
                clear(GREEN, gl);
            });
            if game.global.state == State::Dungeon {
                game.dungeon.render(&mut game.global, &r);
            } else {
                game.city.render(&mut game.global, &r);
            }
            game.global.gl.draw(r.viewport(), |c, gl| {
                text::Text::new_color([0.0, 0.5, 0.0, 1.0], 32)
                    .draw(
                        &score.to_string(),
                        &mut glyph_cache,
                        &DrawState::default(),
                        c.transform.trans(10.0, 100.0),
                        gl,
                    )
                    .unwrap();
            });
            if game.global.state == State::End {
                game.global.gl.draw(r.viewport(), |c, gl| {
                    text::Text::new_color([0.0, 0.5, 0.0, 1.0], 32)
                        .draw(
                            "Game over",
                            &mut glyph_cache,
                            &DrawState::default(),
                            c.transform.trans(WIDTH as f64 / 2.0, HEIGHT as f64 / 2.0),
                            gl,
                        )
                        .unwrap();
                });
            }
        }
        if game.global.state == State::End {
            continue;
        }

        if let Some(u) = e.update_args() {
            game.update(&u);
        }
    }
}
