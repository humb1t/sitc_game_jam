# Cities and Dungeons

C&D is a platform game developed for [SITC Dungeons and Cities game jam](https://itch.io/jam/sitc-dungeons-cities-game-jam).
Game mechanic is based on earning points in dungeons world (while running from daemons) and in case of troubles teleportation into cities world
where you can rest a little (while loosing points).

## Getting started

### Prerequisites

Install [`rust`](https://www.rust-lang.org/install.html).

### How to Build

```bash
$ cargo build
```

### How to Run

```bash
$ cargo run
```

### How to Play

Keyboard         | City        | Dungeon
---------------- | ----------- | -------------------
<kbd>W</kbd>     | Jump        | -
<kbd>S</kbd>     | -           | Jump
<kbd>A</kbd>     | Move left   | Move left
<kbd>D</kbd>     | Move right  | Move right
<kbd>R</kbd>     | Teleport    | Teleport
<kbd>Esc</kbd>   | Exit        | Exit
